import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class BonusTest {

    private boolean arrayIntEqual(int[] arr1, int[] arr2) {
        if(arr1.length != arr2.length) return false;

        for(int i=0; i< arr1.length; i++) {
            if(arr1[i] != arr2[i]) return false;
        }

        return true;
    }

    @Test
    public void testCase1() {

        int[] arr = {1, 2, 3, 4, 5, 8};
        int[] out  = {3};
        int[] output = ArrayUtils.process(arr);
        assertTrue(arrayIntEqual(out, output));

    }

    @Test
    public void testCase2() {

        int[] arr = {1, 2, 3, 4, 5, 8, 6, 3, 3, 6, 10};
        int[] out  = {3, 6, 3};
        int[] output = ArrayUtils.process(arr);
        assertTrue(arrayIntEqual(out, output));

    }

    @Test
    public void testCase3() {

        int[] arr = {12, 11, 9, 3, 5, 8, 6, 3, 1};
        int[] out  = {11, 5, 6};
        int[] output = ArrayUtils.process(arr);
        assertTrue(arrayIntEqual(out, output));

    }

    @Test
    public void testCase4() {

        int[] arr = {1, 2, 3, 4, 5, 8, 6, 3, 3, 2, 2};
        int[] out  = {3, 3};
        int[] output = ArrayUtils.process(arr);
        assertTrue(arrayIntEqual(out, output));

    }

}
