def count_of_sum_pairs(arrayinteger) :
    list_pairs = {}
    total_pairs = 0

    for x in range(len(arrayinteger)):
        current_x = arrayinteger[x]
        for y in range(x+1, len(arrayinteger)):
            current_y = arrayinteger[y]
            sum_current = current_x+current_y
            if str(sum_current) in list_pairs:
                list_pairs[str(sum_current)] = 1 + list_pairs[str(sum_current)]
            else:
                list_pairs[str(sum_current)] = 1

    for numOfPair in list_pairs.values():
        if numOfPair > 1:
            total_pairs += numOfPair

    return total_pairs

