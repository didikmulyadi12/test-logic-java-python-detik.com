# Count Of Equals Sum of Pairs

Given an array, it should return the count of equals sum of pairs.

Example 1, with input [1,2,3,4,5], count of equals sum of pairs in this array is 11, consist of:
Sum of pairs 5, consist of ( 0 , 3 ) and ( 1 , 2 )
Sum of pairs 6, consist of ( 0 , 4 ) and ( 1 , 3 )
Sum of pairs 7, consist of ( 0 , 5 ), ( 1 , 4 ) and ( 2 , 3 )
Sum of pairs 8, consist of ( 1 , 5 ) and ( 2 , 4 )
Sum of pairs 9, consist of ( 2 , 5 ) and ( 3 , 4 )

Example 2, with input [1,1,1,1], all sum of pairs are equals, that is 2. and the count is 6 pairs

## Exception messages

Sometimes it is necessary to raise an exception. When you do this, you should include a meaningful error message to
indicate what the source of the error is. This makes your code more readable and helps significantly with debugging. Not
every exercise will require you to raise an exception, but for those that do, the tests will only pass if you include
a message.

To raise a message with an exception, just write it as an argument to the exception type. For example, instead of
`raise Exception`, you should write:

```python
raise Exception("Meaningful message indicating the source of the error")
```

## Running the tests

To run the tests, run the appropriate command below ([why they are different](https://github.com/pytest-dev/pytest/issues/1629#issue-161422224)):

- Python 2.7: `py.test count_of_sum_pairs_test.py`
- Python 3.4+: `pytest count_of_sum_pairs_test.py`

Alternatively, you can tell Python to run the pytest module (allowing the same command to be used regardless of Python version):
`python -m pytest count_of_sum_pairs_test.py`

### Common `pytest` options

- `-v` : enable verbose output
- `-x` : stop running tests on first failure
- `--ff` : run failures from previous test before running other test cases

For other options, see `python -m pytest -h`

