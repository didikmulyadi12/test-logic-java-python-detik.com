import unittest

from count_of_sum_pairs import count_of_sum_pairs
	
class CountOfEqualSumPairsTest(unittest.TestCase):
    def test_uniform_members(self):
        self.assertEqual(count_of_sum_pairs([1, 1, 1, 1]), 6)

    def test_sequence_members(self):
        self.assertEqual(count_of_sum_pairs([1, 2, 3, 4, 5, 6]), 11)

    def test_non_sequence_members_1(self):
        self.assertEqual(count_of_sum_pairs([1, 3, 5, 9,15]), 0)

    def test_non_sequence_members_2(self):
        self.assertEqual(count_of_sum_pairs([3, 5, 7, 9, 11, 1, 3]), 19)

    def test_non_sequence_members_3(self):
        self.assertEqual(count_of_sum_pairs([3, 15, 7, 9]), 0)

if __name__ == '__main__':
    unittest.main()
