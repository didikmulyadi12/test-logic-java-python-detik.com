# Median Of Multiples

Given a number, find the median (https://en.wikipedia.org/wiki/Median) of all the unique multiples of particular numbers up to
but not including that number.

If we list all the natural numbers below 20 that are multiples of 3 or 5,
we get 3, 5, 6, 9, 10, 12, 15, and 18.

The median of these multiples is (9 + 10)/2 = 9.5 .

If we list all the natural numbers below 150 that are multiples of 5 or 6 or 8,
we get 5 , 6 , 8 , 10 , 12 , 15 , 16 , 18 , 20 , 24 , 25 , 30 , 32 , 35 , 36 , 40 , 42 , 45 , 48 , 50 , 54 , 55 , 56 , 60 , 64 , 65 , 66 , 70 , 72 , 75 , 78 , 80 , 84 , 85 , 88 , 90 , 95 , 96 , 100 , 102 , 104 , 105 , 108 , 110 , 112 , 114 , 115 , 120 , 125 , 126 , 128 , 130 , 132 , 135 , 136 , 138 , 140 , 144 , and 145.

The median of these multiples is 75 .


# Running the tests

You can run all the tests for an exercise by entering

```sh
$ gradle test
```

in your terminal.

## Source

A variation on Problem 1 at Project Euler [http://projecteuler.net/problem=1](http://projecteuler.net/problem=1)

## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have completed the exercise.
