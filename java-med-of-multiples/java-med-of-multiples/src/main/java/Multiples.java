class Multiples {
    private int max;
    private int[] set;

    public Multiples(int max, int[] set) {
        this.max = max;
        this.set = set;
    }

    Float getMedian() {
        int[] listNumber = new int[this.max];
        int index = 0;
        for(int i=1;i<this.max;i++){
            for(int j=0;j<this.set.length;j++){
                if(this.set[j] != 0){
                    if (i % this.set[j] == 0){

                        listNumber[index] = i;
                        index++;
                        break;
                    }
                }
            }
        }

        if(index == 0){
            return Float.valueOf(0);
        }else if(index == 1){
            return Float.valueOf(listNumber[index-1]);
        }else{
            if (index % 2 == 0){
                int medianFirst     = listNumber[(index/2)-1];
                int medianSecond    = listNumber[(index/2)];

                return Float.valueOf(medianFirst+medianSecond)/2;
            }else{

                return Float.valueOf(listNumber[(int) Math.ceil(index/2)]);
            }
        }
    }

}
