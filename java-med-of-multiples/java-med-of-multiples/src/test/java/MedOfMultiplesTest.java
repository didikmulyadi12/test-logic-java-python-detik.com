import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class MedOfMultiplesTest {

    @Test
    public void testNoMultiplesWithinLimit() {

        int[] set = {
            3,
            5
        };
        float output = new Multiples(1, set).getMedian();
        assertEquals(0, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testOneFactorHasMultiplesWithinLimit() {

        int[] set = {
            3,
            5
        };
        float output = new Multiples(4, set).getMedian();
        assertEquals(3, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testMoreThanOneMultipleWithinLimit() {

        int[] set = {
            3
        };
        float output = new Multiples(7, set).getMedian();
        assertEquals(4.5, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testMoreThanOneFactorWithMultiplesWithinLimit() {

        int[] set = {
            3,
            5
        };
        float output = new Multiples(10, set).getMedian();
        assertEquals(5.5, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testEachMultipleIsOnlyCountedOnce() {

        int[] set = {
            3,
            5
        };
        float output = new Multiples(100, set).getMedian();
        assertEquals(50.5, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testAMuchLargerLimit() {

        int[] set = {
            3,
            5
        };
        float output = new Multiples(1000, set).getMedian();
        assertEquals(500.5, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testThreeFactors() {

        int[] set = {
            7,
            13,
            17
        };
        float output = new Multiples(20, set).getMedian();
        assertEquals(13.5, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testFactorsNotRelativelyPrime() {

        int[] set = {
            4,
            6
        };
        float output = new Multiples(15, set).getMedian();
        assertEquals(7, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testSomePairsOfFactorsRelativelyPrimeAndSomeNot() {

        int[] set = {
            5,
            6,
            8
        };
        float output = new Multiples(150, set).getMedian();
        assertEquals(75, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testOneFactorIsAMultipleOfAnother() {

        int[] set = {
            5,
            25
        };
        float output = new Multiples(51, set).getMedian();
        assertEquals(27.5, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testMuchLargerFactors() {

        int[] set = {
            43,
            47
        };
        float output = new Multiples(10000, set).getMedian();
        assertEquals(5008.5, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testAllNumbersAreMultiplesOf1() {

        int[] set = {
            1
        };
        float output = new Multiples(100, set).getMedian();
        assertEquals(50, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testNoFactorsMeanAnEmptySum() {

        int[] set = {};
        float output = new Multiples(10000, set).getMedian();
        assertEquals(0, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testMultiplesOfZeroIsZero() {

        int[] set = {
            0
        };
        float output = new Multiples(1, set).getMedian();
        assertEquals(0, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testFactorZeroDoesNotAffectTheMultiplesOfOtherFactors() {

        int[] set = {
            3,
            0
        };
        float output = new Multiples(4, set).getMedian();
        assertEquals(3, output, 0.0001);

    }

    // @Ignore("Remove to run test")
    @Test
    public void testSolutionsUsingIncludeExcludeMustExtendToCardinalityGreater3() {

        int[] set = {
            2,
            3,
            5,
            7,
            11
        };
        float output = new Multiples(10000, set).getMedian();
        assertEquals(5000.5, output, 0.0001);

    }

}
